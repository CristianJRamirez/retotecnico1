package com.choucair.reto.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class MenuLaptopsPage extends PageObject {
	// Seleccion de Laptops
	@FindBy(xpath = "//a[contains(text(),'HP LP3065')]")
	//*[@id="content"]/div[4]/div[1]/div/div[1]/a
	public WebElementFacade frmCompSel;

	@FindBy(xpath = "//a[contains(text(),'HP LP3065')]")
	//*[@id=\"content\"]/div[4]/div[4]/div
	public WebElementFacade frmUltLin;
	
	// Objetos de la adicion de producto 
	@FindBy(id = "input-option225")
	public WebElementFacade txtDeliveryDate;
	
	@FindBy(id = "input-quantity")
	public WebElementFacade txtQuantity;
	
	@FindBy(id = "button-cart")
	public WebElementFacade btnAddCart;
	
	// Validacion del Carrito
	@FindBy(xpath="//div[contains(@class,'btn-group btn-block' )]")
	//*[@id=\'cart\']/button
	public WebElementFacade btnCartTotal;
	
	@FindBy(xpath = "//ul[contains(@class,'dropdown-menu pull-right' )]//child::a[contains(text(),'HP LP3065')]")
	//*[@id=\'cart\']/ul/li[1]/table/tbody/tr/td[2]/a
	public WebElementFacade txtValidProd;

	@FindBy(xpath = "//ul[contains(@class,'dropdown-menu pull-right' )]//child::strong[contains(text(),' Checkout')]")
	//*[@id=\"cart\"]/ul/li[2]/div/p/a[2]/strong
	public WebElementFacade btnCheckOut;
	
	
	
	
	
}
