package com.choucair.reto.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class FormCarritoPage extends PageObject {

	@FindBy(xpath = "//ul[contains(@class,'dropdown-menu pull-right' )]//child::strong[contains(text(),' View Cart')]")
			 //*[@id=\"cart\"]/ul/li[2]/div/p/a[1]/strong
	public WebElementFacade btnViewCart;

	@FindBy(xpath = "//div[contains(@class,'panel-heading' )]//child::a[contains(text(),'Use Coupon Code ')]")
			 //*[@id=\"accordion\"]/div[1]/div[1]/h4/a
	public WebElementFacade menIngresarCupon;
	
	@FindBy(id="input-coupon")
	public WebElementFacade txtCupon;
	
	@FindBy(id="button-coupon")
	public WebElementFacade btnCupon;
	
	@FindBy(xpath="//DIV[@class='alert alert-danger alert-dismissible']")
			//*[@id='checkout-cart']/div[1]"
	public WebElementFacade lblError;

}
