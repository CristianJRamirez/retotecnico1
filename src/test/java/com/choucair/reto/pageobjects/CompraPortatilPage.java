package com.choucair.reto.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class CompraPortatilPage extends PageObject {

	@FindBy(xpath = "//li[contains(@class,'dropdown' )]//child::a[contains(text(),'Laptops & Notebooks')]")
	
	public WebElementFacade menuLaptops;

	@FindBy(xpath = "//li[contains(@class,'dropdown' )]//child::a[contains(text(),'Show All Laptops & Notebooks')]")
	public WebElementFacade sbmAll;

}
