package com.choucair.reto.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class FormularioClientePage extends PageObject {
	//Google Security
	@FindBy(xpath="//*[@id=\"details-button\"]")
	public WebElementFacade btnSecurity;
	
	@FindBy(xpath="//*[@id=\"proceed-link\"]")
	public WebElementFacade lnkContinuar;
	
	// Registro de cliente	
	@FindBy(id = "button-account")
	public WebElementFacade btnAcount;

	@FindBy(id = "input-payment-firstname")
	public WebElementFacade txtFirstName;
	
	@FindBy(id = "input-payment-lastname")
	public WebElementFacade txtLastName;
	
	@FindBy(id = "input-payment-address-1")
	public WebElementFacade txtAdress1;

	@FindBy(id = "input-payment-email")
	public WebElementFacade txtEmail;
	
	@FindBy(id = "input-payment-telephone")
	public WebElementFacade txtTel;
	
	@FindBy(id = "input-payment-city")
	public WebElementFacade txtCity;
	
	@FindBy(id = "input-payment-postcode")
	public WebElementFacade txtPostCode;
	
	@FindBy(id = "input-payment-password")
	public WebElementFacade txtPassword;

	@FindBy(id = "input-payment-confirm")
	public WebElementFacade txtPassConfirm;
	
	@FindBy(id = "input-payment-country")
	public WebElementFacade cbxCountry;
	
	@FindBy(id = "input-payment-zone")
	public WebElementFacade cbxState;
	
	@FindBy(xpath="//div[contains(@class,'buttons clearfix' )]//child::input[@type='checkbox']")
	public WebElementFacade chkPolicy;
	
	@FindBy(id = "button-register")
	public WebElementFacade btnRegister;
	
	@FindBy(id = "button-shipping-address")
	public WebElementFacade btnConfDir;
	
	@FindBy(name = "comment")
	public WebElementFacade txtComentario;
	
	@FindBy(id = "button-shipping-method")
	public WebElementFacade btnGoPay;
	
	@FindBy(id = "button-payment-method")
	public WebElementFacade btnPay;
	
	@FindBy(xpath = "//div[contains(@class,'pull-right' )]//child::input[@type='checkbox']")
			//*[@id=\"collapse-payment-method\"]/div/div[2]/div/input[1]
	public WebElementFacade chkPay;
	
	@FindBy(id = "button-confirm")
	public WebElementFacade btnConfOrder;
	 
	//Mensaje de Confirmacion
		@FindBy(id = "content")
	public WebElementFacade lblConfirm;
	
}
