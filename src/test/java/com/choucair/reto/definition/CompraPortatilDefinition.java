package com.choucair.reto.definition;

import java.util.List;

import com.choucair.reto.steps.CompraPortatilSteps;
import com.choucair.reto.steps.PagoPortatilSteps;

import cucumber.api.DataTable;
import cucumber.api.java.en.*;
import net.thucydides.core.annotations.Steps;

public class CompraPortatilDefinition {
	
	@Steps
	CompraPortatilSteps compraPortatilSteps;
	
	@Steps
	PagoPortatilSteps pagoPortatilSteps;

@Given("^Ingreso a la tienda YourStore$")
public void ingreso_a_la_tienda_YourStore() {
	compraPortatilSteps.abrir_navegador();
}

@When("^Selecciono el menu (.*) y Selecciono el submenu (.*)$")
public void selecciono_el_menu_y_Selecciono_el_submenu(String arg1, String arg2) {
	compraPortatilSteps.seleccionar_menu();
}

@When("^imprimo el listado de portatil, descripcion y precio$")
public void imprimo_el_listado_de_portatil_descripcion_y_precio() {
	compraPortatilSteps.genera_lista();
}

@When("^Selecciono el Producto en la lista$")
public void Selecciono_el_Producto_en_la_lista() {
		compraPortatilSteps.seleccionar_laptop();
}

@When("^añado el producto al carrito de compras$")
public void añado_el_producto_al_carrito_de_compras() {
	compraPortatilSteps.agregar_laptop_carrito();
}

@Then("^Valido que el producto se haya añadido al carrito de compras$")
public void valido_que_el_producto_se_haya_añadido_al_carrito_de_compras() {
	compraPortatilSteps.valida_carrito();
	pagoPortatilSteps.googlesecurity();
}

@Then("^lleno el formulario de registro de Cliente$")
public void lleno_el_formulario_de_registro_de_Cliente(DataTable dtDatosForm) {
	List<List<String>> data = dtDatosForm.raw();
	pagoPortatilSteps.seleccionar_boton_registro();
	
	for(int i=1; i<data.size(); i++) {	
		pagoPortatilSteps.llena_datos_personales(data, i);
		pagoPortatilSteps.llena_correspondencia(data, i);
		try {
			Thread.sleep(5000);
		}catch(InterruptedException e){
		}
	}
	pagoPortatilSteps.acepto_terminos_y_confirma_direccion();
	pagoPortatilSteps.comentarios_de_la_entrega_y_pago();
}


@Then("^Valido que el pedido se haya efectuado$")
public void Valido_que_el_pedido_se_haya_efectuado() {
	pagoPortatilSteps.confirmacion_de_pedidos();
}

//Caso Alterno


@When("^abro el carrito de compras e ingreso el cupon$")
public void abro_el_carrito_de_compras_e_ingreso_el_cupon() {
	compraPortatilSteps.abrecarrito();
	compraPortatilSteps.ingresaCupon();
}

@Then("^La tienda me notifica que el cupon no es valido$")
public void la_tienda_me_notifica_que_el_cupon_no_es_valido() {
	compraPortatilSteps.valida_error();
	
}













}
