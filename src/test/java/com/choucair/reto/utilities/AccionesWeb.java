package com.choucair.reto.utilities;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class AccionesWeb extends PageObject {

	private String bordeRojo = "arguments[0].style.border='3px dashed red'";
	private String bordeVerde = "arguments[0].style.border='3px dashed green'";
	/**
	 * Este metodo espera a que un elemento este visible
	 * 
	 * @param xpath elemento que se requiere esperar a que sea visible
	 * 
	 * @return retorna true o false si el elemento esta o no presente
	 */
	public boolean esperoElementoVisible(By xpath) {
		try {
			WebDriverWait espera = new WebDriverWait(getDriver(), 50);
			espera.until(ExpectedConditions.visibilityOfElementLocated(xpath));
			if (element(xpath).isCurrentlyVisible()) {
				return true;
			}
		} catch (Exception e) {
			System.out.print("en la clase AccionesWeb en el metodo esperoElementoVisible" + e);
		}
		return false;
	}

	/**
	 * 
	 * @param xpath
	 * @return
	 */
	public boolean esperoElementoVisible(String strXpath) {
		try {
			WebDriverWait espera = new WebDriverWait(getDriver(), 50);
			espera.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(strXpath)));
			if (element(strXpath).isCurrentlyVisible()) {
				return true;
			}
		} catch (Exception e) {
			System.out.print("en la clase AccionesWeb en el metodo esperoElementoVisible" + e);
		}
		return false;
	}

	/**
	 * Este metodo espera a que un elemento este visible
	 * 
	 * @param WebElementFacade elemento que se requiere esperar a que sea visible
	 * @return retorna true o false si el elemento esta o no presente
	 */
	public boolean esperoElementoVisible(WebElementFacade wbElemento) {
		try {
			element(wbElemento).waitUntilVisible();
			if (element(wbElemento).isCurrentlyVisible()) {
				return true;
			}
		} catch (Exception e) {
			System.out.print("en la clase AccionesWeb en el metodo esperoElementoVisible" + e);
		}
		return false;
	}

	/**
	 * Este metodo espera a que un elemento este Habilitado
	 * 
	 * @param xpath elemento que se requiere esperar a que sea visible
	 * @return retorna true o false si el elemento esta o no presente
	 */
	public boolean esperoElementoHabilitado(By xpath) {
		try {
			WebDriverWait espera = new WebDriverWait(getDriver(), 50);
			espera.until(ExpectedConditions.visibilityOfElementLocated(xpath));
			if (element(xpath).isCurrentlyEnabled()) {
				return true;
			}

		} catch (Exception e) {
			System.out.print("en la clase AccionesWeb en el metodo esperoElementoHabilitado " + e);
		}
		return false;
	}

	/**
	 * 
	 * @param xpath
	 * @return
	 */
	public boolean esperoElementoHabilitado(String strXpath) {
		try {
			WebDriverWait espera = new WebDriverWait(getDriver(), 50);
			espera.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(strXpath)));
			if (element(strXpath).isCurrentlyEnabled()) {
				return true;
			}

		} catch (Exception e) {
			System.out.print("en la clase AccionesWeb en el metodo esperoElementoHabilitado " + e);
		}
		return false;
	}

	/**
	 * Este metodo espera a que un elemento este Habilitado
	 * 
	 * @param WebElementFacader elemento que se requiere esperar a que sea visible
	 * @return retorna true o false si el elemento esta o no presente
	 */
	public boolean esperoElementoHabilitado(WebElementFacade wbElemento) {
		try {
			element(wbElemento).waitUntilEnabled();
			if (element(wbElemento).isCurrentlyEnabled()) {
				return true;
			}

		} catch (Exception e) {
			System.out.print("en la clase AccionesWeb en el metodo esperoElementoHabilitado " + e);
		}
		return false;
	}

	public void click(WebElementFacade wbElement, boolean blnTomarPantalla) {
		bordearElemento(wbElement);
		esperoElementoVisible(wbElement);
		esperoElementoHabilitado(wbElement);
		if (blnTomarPantalla) {
			Serenity.takeScreenshot();
		}
		wbElement.click();
	}

	public void click(String strxpath, boolean blnTomarPantalla) {
		esperoElementoVisible(strxpath);
		esperoElementoHabilitado(strxpath);
		if (blnTomarPantalla) {
			Serenity.takeScreenshot();
		}
		find(By.xpath(strxpath)).click();
	}

	/**
	 * Metodo encargado de abrir la url
	 *
	 * @param strURL
	 */
	public void abrirURL(String strURL) {
		openAt(strURL);
		espera_implicita(2);
		getDriver().navigate().refresh();
		// getDriver().manage().deleteAllCookies();
	}

	/**
	 * Método de una aserción la cual valida que el elemeto contenga un texto
	 *
	 * @param mensaje
	 * @param elemet
	 * @param textoaComparar
	 */
	public void validarTextoDeUnElemetoContieneTexto(WebElementFacade elemet, String textoaComparar, String mensaje) {
		String textoElemet = element(elemet).getText();
		Serenity.takeScreenshot();
		assertTrue(mensaje, textoElemet.contains(textoaComparar));

	}

	/**
	 * Metodo generico para las funciones de limpiar campos y escribir
	 * 
	 * @param wbElement
	 * @param dato
	 */
	public void clear_sendKeys(WebElementFacade wbElement, String dato, boolean blnTomarPantalla) {
		bordearElemento(wbElement);
		wbElement.click();
		wbElement.clear();
		wbElement.sendKeys(dato);

		if (blnTomarPantalla) {
			Serenity.takeScreenshot();
		}
	}

	/**
	 * 
	 * @param webElementFac
	 * @param bordearElemento
	 * @param tomarScreenshot
	 */
	public void moverPantallahastaElemento(WebElementFacade webElementFac, boolean bordearElemento,
			boolean tomarScreenshot) {

		try {
			esperoElementoVisible(webElementFac);
			esperoElementoHabilitado(webElementFac);
			((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView();", webElementFac);
		} catch (Exception ex) {
			System.out.println("No se movio la pantalla hacia el elemento con xpath: " + webElementFac);
		}
		if (bordearElemento)
			bordearElemento(webElementFac);
		if (tomarScreenshot)
			Serenity.takeScreenshot();
	}

	/**
	 * Este metodo crea un borde rojo sobre un elemento de la pagina
	 * 
	 * @param strXptElemento xpath del elemento que se bordea
	 */
	public void bordearElemento(String strXptElemento) {
		try {
			WebElement webElemeBordear = find(By.xpath(strXptElemento));
			((JavascriptExecutor) getDriver()).executeScript(bordeRojo, webElemeBordear);
		} catch (Exception ex) {
			System.out.println("No se pudo bordear el elemento con xpath: " + strXptElemento);
		}
	}

	/**
	 * Este metodo crea un borde rojo sobre un elemento de la pagina
	 * 
	 * @param webElementFac Elemento que se bordeara
	 */
	public void bordearElemento(WebElementFacade webElementFac) {
		try {
			((JavascriptExecutor) getDriver()).executeScript(bordeRojo, webElementFac);
		} catch (Exception ex) {
			System.out.println("No se pudo bordear el elemento con xpath: " + webElementFac);
		}
	}

	/**
	 * Mueve la pantalla hasta la parte final de la pagina
	 */
	public void moverScrollHastaFinal() {

		try {
			((JavascriptExecutor) getDriver()).executeScript("window.scrollTo(0, document.body.scrollHeight)");
		} catch (Exception jE) {
			System.out.println("No Movio la pantalla: " + jE.getMessage());
		}
	}

	/**
	 * Metodo para una espera
	 * 
	 * @param segundos
	 */
	public void espera_implicita(int segundos) {
		waitFor(segundos).second();
	}

	public void recorrer_productos(String pathg, String pathmarca1, String pathmarca2, String pathprecio1,
			String pathprecio2, String pathdescripcion1, String pathdescripcion2) {
		waitFor(5).second();
		List<WebElementFacade> allProduct = findAll(pathg);
		String strMarca = "";
		String strPrecio = "";
		String strDescripcion = "";
		for (int cont = 1; cont < allProduct.size(); cont++) {

			strMarca = find(By.xpath(pathmarca1 + cont + pathmarca2)).getText();
			strPrecio = find(By.xpath(pathprecio1 + cont + pathprecio2)).getText();
			strDescripcion = find(By.xpath(pathdescripcion1 + cont + pathdescripcion2)).getText();

			System.out.println("\n" + strMarca + " " + strDescripcion + " " + strPrecio);
			System.out.println("*********************************************************************");
		}
	}

	public void seleccione_de_lista(WebElementFacade valor, String dato) {
		(valor).selectByVisibleText(dato);
	}

	public void confirma_notificacion_googlesecurity(WebElementFacade valor1, WebElementFacade valor2) {
		valor1.click();
		valor2.click();
	}

	public void form_con_errores(WebElementFacade texto) {
		espera_implicita(2);
		assertThat(texto.isCurrentlyVisible(), is(true));
		espera_implicita(2);
		Serenity.takeScreenshot();
	}
	
/** Este metodo espera que el titulo cargue, para que los objetos de la pagina esten visibles
 * @param titulo ingresa el nombre de la pagina que debe esperar
 */
	public void esperaTitulo(String titulo) {
		try {
			WebDriverWait espera = new WebDriverWait(getDriver(), 50);
			espera.until(ExpectedConditions.titleContains(titulo));
		} catch (Exception e) {
			System.out.print("en la clase AccionesWeb en el metodo esperoElementoClick" + e);
		}

	}

	public void esperaElementoClickeable(WebElementFacade wbElement) {
		try {
			WebDriverWait espera = new WebDriverWait(getDriver(), 50);
			espera.until(ExpectedConditions.elementToBeClickable(wbElement));
		} catch (Exception e) {
			System.out.print("en la clase AccionesWeb en el metodo EsperaElementoClickeable" + e);
		}
	}

}
