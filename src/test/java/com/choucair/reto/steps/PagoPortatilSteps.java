package com.choucair.reto.steps;


import java.util.List;

import com.choucair.reto.pageobjects.FormularioClientePage;
import com.choucair.reto.pageobjects.MenuLaptopsPage;
import com.choucair.reto.utilities.AccionesWeb;

public class PagoPortatilSteps {
	
	AccionesWeb accionesWeb;
	MenuLaptopsPage menuLaptopsPage;
	FormularioClientePage formularioClientePage;
	
	public void seleccionar_boton_registro() {
		accionesWeb.bordearElemento(formularioClientePage.btnAcount);
		accionesWeb.click(formularioClientePage.btnAcount, false);
	}
	
	
	public void googlesecurity() {
		accionesWeb.confirma_notificacion_googlesecurity(formularioClientePage.btnSecurity, formularioClientePage.lnkContinuar);
	}
	
	
	
	public void llena_datos_personales(List<List<String>> data, int id) {
		accionesWeb.clear_sendKeys(formularioClientePage.txtFirstName, data.get(id).get(0).trim(),false);
		accionesWeb.clear_sendKeys(formularioClientePage.txtLastName, data.get(id).get(1).trim(),false);	
		accionesWeb.clear_sendKeys(formularioClientePage.txtEmail,data.get(id).get(2).trim(),false);
		accionesWeb.clear_sendKeys(formularioClientePage.txtTel, data.get(id).get(3).trim(),false);
		accionesWeb.clear_sendKeys(formularioClientePage.txtPassword, data.get(id).get(4).trim(),false);
		accionesWeb.clear_sendKeys(formularioClientePage.txtPassConfirm, data.get(id).get(5).trim(),false);
	}
	
	public void llena_correspondencia(List<List<String>> data, int id) {
		accionesWeb.clear_sendKeys(formularioClientePage.txtAdress1, data.get(id).get(6).trim(),false);
		accionesWeb.clear_sendKeys(formularioClientePage.txtCity, data.get(id).get(7).trim(),false);	
		accionesWeb.clear_sendKeys(formularioClientePage.txtPostCode, data.get(id).get(8).trim(),false);
		accionesWeb.seleccione_de_lista(formularioClientePage.cbxCountry, data.get(id).get(9).trim());
		accionesWeb.seleccione_de_lista(formularioClientePage.cbxState, data.get(id).get(10).trim());
	}
	
	public void acepto_terminos_y_confirma_direccion() {
		accionesWeb.click(formularioClientePage.chkPolicy, false);
		accionesWeb.click(formularioClientePage.btnRegister, true);
		accionesWeb.esperaElementoClickeable(formularioClientePage.btnConfDir);
		accionesWeb.click(formularioClientePage.btnConfDir, false);	
	}
	
	public void comentarios_de_la_entrega_y_pago() {
		accionesWeb.clear_sendKeys(formularioClientePage.txtComentario, "Dejar en Porteria",false);
		accionesWeb.click(formularioClientePage.btnGoPay, false);
		accionesWeb.click(formularioClientePage.chkPay, false);
		accionesWeb.click(formularioClientePage.btnPay, false);
		accionesWeb.click(formularioClientePage.btnConfOrder, true);
		accionesWeb.espera_implicita(5);
	}
	
	public void confirmacion_de_pedidos() {
		accionesWeb.esperaTitulo("Your order has been placed!");
		accionesWeb.validarTextoDeUnElemetoContieneTexto(formularioClientePage.lblConfirm, "Your order has been placed!", "Your order has been placed!");
		accionesWeb.espera_implicita(5);
	}
	
	
	

}
