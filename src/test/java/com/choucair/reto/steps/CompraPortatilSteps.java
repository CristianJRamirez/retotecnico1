package com.choucair.reto.steps;

import com.choucair.reto.pageobjects.CompraPortatilPage;
import com.choucair.reto.pageobjects.FormCarritoPage;
import com.choucair.reto.pageobjects.MenuLaptopsPage;
import com.choucair.reto.utilities.AccionesWeb;

public class CompraPortatilSteps {
	
	AccionesWeb accionesWeb;
	CompraPortatilPage compraPortatilPage;
	MenuLaptopsPage menuLaptopsPage;
	FormCarritoPage formCarritoPage;
	
	public void abrir_navegador() {
			accionesWeb.abrirURL("http://opencart.abstracta.us/index.php?route=common/home");	
	}
	
	public void seleccionar_menu() {
		//Selecciona el Menu y submenu
			accionesWeb.click(compraPortatilPage.menuLaptops, false);
			accionesWeb.click(compraPortatilPage.sbmAll, true);		
	}
	

	public void genera_lista() {
		//ScrollDown
					accionesWeb.recorrer_productos("//*[@id=\"content\"]/div[4]/div", 
					"//*[@id=\"content\"]/div[4]/div[", "]/div/div[2]/div[1]/h4/a", 
					"//*[@id=\"content\"]/div[4]/div[", "]/div/div[2]/div[1]/p[2]", 
					"//*[@id=\"content\"]/div[4]/div[", "]/div/div[2]/div[1]/p[1]");
    }
	
	public void seleccionar_laptop() {
		//Selecciona el Portatil
		accionesWeb.moverPantallahastaElemento(menuLaptopsPage.frmCompSel, false, false);
		accionesWeb.click(menuLaptopsPage.frmCompSel, true);		
    }
	
	public void agregar_laptop_carrito() {
		//Agregar Laptop al carrito
		accionesWeb.clear_sendKeys(menuLaptopsPage.txtDeliveryDate, "2021-08-25",false);
		accionesWeb.clear_sendKeys(menuLaptopsPage.txtQuantity, "1",false);
		accionesWeb.moverPantallahastaElemento(menuLaptopsPage.btnAddCart, false, false);
		accionesWeb.click(menuLaptopsPage.btnAddCart, true);
    }
	
	
	public void valida_carrito() {
		//Valida que se haya agregado el producto al carrito
		accionesWeb.esperaElementoClickeable(menuLaptopsPage.btnCartTotal);
		accionesWeb.click(menuLaptopsPage.btnCartTotal, false);
		accionesWeb.validarTextoDeUnElemetoContieneTexto(menuLaptopsPage.txtValidProd, "HP LP3065", "HP LP3065");
		accionesWeb.click(menuLaptopsPage.btnCheckOut, false);
	}
	 
	
	public void abrecarrito() {
		accionesWeb.esperaElementoClickeable(menuLaptopsPage.btnCartTotal);
		accionesWeb.click(menuLaptopsPage.btnCartTotal, false);
		accionesWeb.click(formCarritoPage.btnViewCart,false);
	}
	
	public void ingresaCupon() {
		accionesWeb.click(formCarritoPage.menIngresarCupon, false);
		accionesWeb.clear_sendKeys(formCarritoPage.txtCupon, "ChoucairTesting",false);
		accionesWeb.click(formCarritoPage.btnCupon, true);
	}
	
	public void valida_error() {
		accionesWeb.form_con_errores(formCarritoPage.lblError);
	}
	
	
}
