@tiendaComputadores
Feature: Compra de producto en pagina web
  El usuario debe poder seleccionar el producto de su eleccion
  se deben listar los productos disponibles para la compra, con sus especificaciones
 
@CasoExitoso
Scenario: Compra de producto en la pagina web
					debe permitir añadir el producto al carrito de compra y realizar la compra
  	Given Ingreso a la tienda YourStore
		When Selecciono el menu Laptops & Notebooks y Selecciono el submenu Show All Laptos & Notebooks
		And imprimo el listado de portatil, descripcion y precio
		And Selecciono el Producto en la lista
    And añado el producto al carrito de compras
    Then Valido que el producto se haya añadido al carrito de compras
    And lleno el formulario de registro de Cliente
    |Nombre|Apellido|Mail|Telefono|Password1|Password2|Direccion|City|Cp|Pais|Estado|
		|Cristian|Ramirez|cristiananramr@choucairtestsing.com|3216547890|valor1|valor1|Avenida 3 4-56|Tunja|150001|Colombia|Boyaca|
   	And Valido que el pedido se haya efectuado
  
    
  @CasoAlterno
  Scenario: Compra de producto con cupon de descuento no valido
 	 					Se compra un producto con un cupon no valido debe notificar que el cupon no esta vigente
 	 	Given Ingreso a la tienda YourStore				
  	When Selecciono el menu Laptops & Notebooks y Selecciono el submenu Show All Laptos & Notebooks
  	And Selecciono el Producto en la lista
  	And añado el producto al carrito de compras
    And abro el carrito de compras e ingreso el cupon
    Then La tienda me notifica que el cupon no es valido